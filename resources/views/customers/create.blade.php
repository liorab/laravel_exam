@extends('layouts.app')
@section('content')
<h1>Create a new customer</h1>
<form method='post' action="{{action('CustomerController@store')}}">
    {{csrf_field()}}
    <div class="form-group">
        <label for ="name"> what is his/ her name?</label>
        <input type="text" class= "form-control" name="name">

        <label for ="phone"> what is his/ her phone?</label>
        <input type="text" class= "form-control" name="phone">

        <label for ="email"> what is his/ her email?</label>
        <input type="text" class= "form-control" name="email">
    </div>
    <div class = "form-group">
        <input type="submit" class="form-control" name="submit" value="save">
    </div>
</form>
@endsection